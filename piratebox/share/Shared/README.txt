
	</div>
</div>
<footer id="about">
	<div class="container">
		<p class="to-top"><a href="#header">Haut de page</a></p>
		<p>
			<h2>Licence</h2>
			Sauf mention contraire explicite, tous les contenus de ce dispositif sont mis &agrave; disposition sous les termes de la licence <a>CC-0</a> <i>(Creative Commons Z&eacute;ro - Domaine Public)</i>. C'est &agrave; dire que vous &ecirc;tes autoris&eacute; &agrave; partager et adapter ces contenus pour toute utilisation (m&ecirc;me commerciale), sans aucune restriction.
		</p>
	</div>
</footer>
