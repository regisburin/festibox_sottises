# FestiBox
![logo](https://framagit.org/regisburin/festibox_sottises/raw/master/piratebox/share/content/img/piratebox-logo-horizontal-black.png)    
**Dispositif Autonome Nomade et Éphémère de Partages Numériques**    
_Mise en oeuvre pour le festival "La Fête Des Sottises" (21-23 juillet 2017 - Salies-de-Béarn 64)_

Dérivée de la [PirateBox](https://piratebox.cc/) de David Darts, la FestiBox propose un espace ouvert et anonyme d'échange de fichiers et de messages.

Projet porté par **[La Fab'Brique](http://lafabbrique.org)**,  FabLab associatif basé à Sauveterre-de-Béarn (Fr - 64).

----
## Installation réalisée sur un RaspberryPi 3B (base Archlinux)

Informations et notes compémentaires sur le projet: [voir le wiki de La Fab'Brique](http://lafabbrique.org/wiki/projets:festibox)
